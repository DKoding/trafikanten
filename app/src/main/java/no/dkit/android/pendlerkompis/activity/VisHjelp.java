package no.dkit.android.pendlerkompis.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import no.dkit.android.pendlerkompis.R;

public class VisHjelp extends Activity {
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.hjelp);
    }
}
