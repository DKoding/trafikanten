package no.dkit.android.pendlerkompis.gui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.json.dto.Avgang;

public class VisAvgangerAdapter<T extends Avgang> extends AvgangAdapter<T> {
    private static final int TABLEROW = 0;
    public static final int LINJE = 0;
    public static final int TIL = 1;
    public static final int AVGANG = 2;

    LocalTime now;
    LocalTime current;

    public VisAvgangerAdapter(Context context, int i) {
        super(context, i);
        now = new LocalTime();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TableLayout tableLayout;

        if (convertView == null) {
            tableLayout = (TableLayout) View.inflate(getContext(), R.layout.visavganger_item, null);
        } else {
            tableLayout = (TableLayout) convertView;
        }

        final TableRow tableRow = (TableRow) tableLayout.getChildAt(TABLEROW);

        final TextView line = (TextView) tableRow.getChildAt(LINJE);
        final TextView destination = (TextView) tableRow.getChildAt(TIL);
        final TextView departure = (TextView) tableRow.getChildAt(AVGANG);

        final T avgang;

        if (filter.equals(NO_FILTER)) {
            avgang = avgangListe.get(position);
        } else {
            avgang = filtrerteAvganger.get(position);
        }

        line.setText(avgang.getMonitoredVehicleJourney().getPublishedLineName());

        String expectedDepartureTime = avgang.getMonitoredVehicleJourney().getMonitoredCall().getExpectedDepartureTime();
        current = now.withHourOfDay(Integer.parseInt(expectedDepartureTime.substring(11, 13)))
                .withMinuteOfHour(Integer.parseInt(expectedDepartureTime.substring(14, 16)));

        int minutes = Minutes.minutesBetween(now, current).getMinutes();

        if (minutes == 0)
            departure.setText(minutes + " nå");
        else if (minutes <= 15)
            departure.setText(minutes + " min");
        else
            departure.setText(String.format("%02d:%02d", current.getHourOfDay(), current.getMinuteOfHour()));

        destination.setText(avgang.getMonitoredVehicleJourney().getDestinationName());

        if (position % 2 == TABLEROW)
            tableLayout.setBackgroundColor(getContext().getResources().getColor(R.color.linecolor));
        if (position % 2 == 1)
            tableLayout.setBackgroundColor(getContext().getResources().getColor(R.color.altlinecolor));

        return tableLayout;
    }
}