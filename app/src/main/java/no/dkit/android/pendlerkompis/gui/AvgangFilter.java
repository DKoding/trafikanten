package no.dkit.android.pendlerkompis.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class AvgangFilter extends EditText {
    VisAvgangerAdapter adapter;

    public AvgangFilter(Context context) {
        super(context);
    }

    public AvgangFilter(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AvgangFilter(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent keyEvent) {
        if (adapter != null) {
            adapter.setTextFilter(getText().toString());
            adapter.updateFilteredItems();
        }

        return super.onKeyUp(keyCode, keyEvent);
    }

    public void setListeAdapter(VisAvgangerAdapter listeAdapter) {
        this.adapter = listeAdapter;
    }
}