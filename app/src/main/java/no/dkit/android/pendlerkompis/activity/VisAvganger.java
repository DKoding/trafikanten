package no.dkit.android.pendlerkompis.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;

import java.util.ArrayList;

import no.dkit.android.pendlerkompis.Config;
import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.gui.AvgangFilter;
import no.dkit.android.pendlerkompis.gui.VisAvgangerAdapter;
import no.dkit.android.pendlerkompis.http.AsyncHttpRequest;
import no.dkit.android.pendlerkompis.json.AvgangParser;
import no.dkit.android.pendlerkompis.json.dto.Avgang;

public class VisAvganger extends ListActivity {
    ArrayList<Avgang> avgangListe = new ArrayList<Avgang>();
    VisAvganger myContext;
    AvgangFilter avgangFilter;
    TextView stopName;
    VisAvgangerAdapter<Avgang> avgangAdapter;
    private String filterText = "";
    private Integer fromId = -1;
    private String fromName = "";

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        myContext = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.visavganger);

        avgangAdapter = new VisAvgangerAdapter<>(myContext, 0);
        avgangFilter = (AvgangFilter) findViewById(R.id.Filter);
        stopName = (TextView) findViewById(R.id.StoppeStedNavn);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            fromId = extras.getInt(Config.ID_KEY);
            fromName = extras.getString(Config.NAME_KEY);
            stopName.setText(getString(R.string.fra) + " " + fromName);
            getAvganger(fromId);
        }
    }

    private void getAvganger(Integer fromId) {
        final ProgressDialog dialog = ProgressDialog.show(VisAvganger.this, getString(R.string.wait_soker_avganger),
                getString(R.string.wait_soker_avganger), true, true);
        new AsyncHttpRequest(Config.AVGANGER_URL + fromId, new Handler() {
            public void handleMessage(Message msg) {
                String avgangXML = msg.getData().getString(AsyncHttpRequest.RESPONSE);
                if (avgangXML.equals("")) {
                    dialog.dismiss();
                    new AlertDialog.Builder(myContext)
                            .setMessage(getResources().getText(R.string.network_error))
                            .setNeutralButton(getResources().getText(R.string.ok), null)
                            .show();
                } else {
                    setupAvgangListe(dialog, avgangXML);
                }
            }
        });
    }

    private void setupAvgangListe(final ProgressDialog dialog, final String avgangJson) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                avgangListe = new AvgangParser().parseData(avgangJson);

                if (avgangListe.size() == 0) {
                    dialog.dismiss();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(myContext)
                                    .setMessage(getResources().getText(R.string.ingen_avganger))
                                    .setNeutralButton(getResources().getText(R.string.ok), null)
                                    .show();
                        }
                    });
                } else {
                    avgangAdapter.setAvganger(avgangListe);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            avgangFilter.setText(filterText);
                            avgangFilter.getText().setFilters(new InputFilter[]{new InputFilter.AllCaps()});
                            avgangFilter.setListeAdapter(avgangAdapter);
                            setListAdapter(avgangAdapter);
                        }
                    });

                    dialog.dismiss();
                }

                return null;
            }
        }.execute();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, R.string.oppdater, 1, myContext.getResources().getString(R.string.oppdater));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.string.oppdater) {
            if (fromId != null && fromId != -1)
                getAvganger(fromId);
        }

        return super.onOptionsItemSelected(item);
    }
}
