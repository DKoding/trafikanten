
package no.dkit.android.pendlerkompis.json.dto;

import java.util.List;

public class OccupancyData{
   	private boolean occupancyAvailable;
   	private Number occupancyPercentage;

 	public boolean getOccupancyAvailable(){
		return this.occupancyAvailable;
	}
	public void setOccupancyAvailable(boolean occupancyAvailable){
		this.occupancyAvailable = occupancyAvailable;
	}
 	public Number getOccupancyPercentage(){
		return this.occupancyPercentage;
	}
	public void setOccupancyPercentage(Number occupancyPercentage){
		this.occupancyPercentage = occupancyPercentage;
	}
}
