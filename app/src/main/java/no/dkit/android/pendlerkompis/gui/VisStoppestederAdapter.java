package no.dkit.android.pendlerkompis.gui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.json.dto.Places;

public class VisStoppestederAdapter<T extends Places> extends StoppestedAdapter<T> {
    public static final int TABLEROW = 0;
    public static final int NAVN = 0;
    public static final int DISTRIKT = 1;

    public VisStoppestederAdapter(Context context, int i) {
        super(context, i);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TableLayout tableLayout;

        if (convertView == null) {
            tableLayout = (TableLayout) View.inflate(getContext(), R.layout.visstoppesteder_item, null);
        } else {
            tableLayout = (TableLayout) convertView;
        }

        final TableRow tableRow = (TableRow) tableLayout.getChildAt(TABLEROW);

        final TextView navn = (TextView) tableRow.getChildAt(NAVN);
        final TextView distrikt = (TextView) tableRow.getChildAt(DISTRIKT);

        final T stoppested;

        stoppested = stoppestedListe.get(position);

        navn.setText(stoppested.getName());
        distrikt.setText(stoppested.getDistrict());

        if (position % 2 == 0) tableLayout.setBackgroundColor(getContext().getResources().getColor(R.color.linecolor));
        if (position % 2 == 1) tableLayout.setBackgroundColor(getContext().getResources().getColor(R.color.altlinecolor));

        return tableLayout;
    }
}