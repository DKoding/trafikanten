package no.dkit.android.pendlerkompis.http;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AsyncHttpRequest extends Thread {
    public static final String ENCODING = "UTF-8";
    private final String mURL;
    private final Handler mHandler;
    public static final String RESPONSE = "response";

    public AsyncHttpRequest(String url, Handler handler) {
        super();
        mURL = url;
        mHandler = handler;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO)
            System.setProperty("http.keepAlive", "false");

        start();
    }

    public void run() {
        Bundle b = new Bundle();
        HttpURLConnection conn = null;

        try {
            conn = (HttpURLConnection) (new URL(mURL)).openConnection();
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
/*
            conn.setDoInput(true);
            conn.setDoOutput(true);
*/
            conn.connect();
            b.putString(RESPONSE, toString(conn.getInputStream()));
            conn.disconnect();
        } catch (IOException e) {
            b.putString(RESPONSE, "");
        } finally {
            if (conn != null)
                conn.disconnect();
        }

        if (mHandler != null) {
            Message msg = mHandler.obtainMessage();
            msg.setData(b);
            mHandler.sendMessage(msg);
        }
    }

    private static String toString(InputStream inputStream) throws IOException {
        StringBuilder outputBuilder = new StringBuilder(8192);
        String string;
        if (inputStream != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, ENCODING));
            while (null != (string = reader.readLine())) {
                outputBuilder.append(string);
            }
        }
        return outputBuilder.toString();
    }
}
