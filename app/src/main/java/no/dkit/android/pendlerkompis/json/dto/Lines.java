
package no.dkit.android.pendlerkompis.json.dto;

import java.util.List;

public class Lines{
   	private Number iD;
   	private String lineColour;
   	private String name;
   	private Number transportation;

 	public Number getID(){
		return this.iD;
	}
	public void setID(Number iD){
		this.iD = iD;
	}
 	public String getLineColour(){
		return this.lineColour;
	}
	public void setLineColour(String lineColour){
		this.lineColour = lineColour;
	}
 	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
 	public Number getTransportation(){
		return this.transportation;
	}
	public void setTransportation(Number transportation){
		this.transportation = transportation;
	}
}
