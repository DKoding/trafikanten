package no.dkit.android.pendlerkompis.activity;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListView;

import java.util.ArrayList;

import no.dkit.android.pendlerkompis.Config;
import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.gui.VisStoppestederAdapter;
import no.dkit.android.pendlerkompis.json.dto.Places;
import no.dkit.android.pendlerkompis.util.TextHelper;

public class VisStoppesteder extends ListActivity {
    ArrayList<Places> stoppestedListe = new ArrayList<Places>();

    VisStoppesteder myContext;
    static SharedPreferences prefs;

    VisStoppestederAdapter<Places> stoppestedAdapter;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        myContext = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.visstoppesteder);

        stoppestedAdapter = new VisStoppestederAdapter<Places>(myContext, 0);

        hentLagredeStoppesteder();
    }

    private void hentLagredeStoppesteder() {
        prefs = getSharedPreferences(Config.PREFS_NAME, 0);
        String stoppestedString = prefs.getString(Config.PREFS_STOPPESTEDER, "");

        stoppestedListe = TextHelper.stringTilStoppesteder(stoppestedString);
        stoppestedAdapter.setStoppesteder(stoppestedListe);

        setListAdapter(stoppestedAdapter);
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        Intent myIntent = new Intent(myContext, VisAvganger.class);
        myIntent.putExtra(Config.ID_KEY, stoppestedAdapter.getItem(position).getiD());
        myIntent.putExtra(Config.NAME_KEY, stoppestedAdapter.getItem(position).getName());
        startActivityForResult(myIntent, 0);
    }
}