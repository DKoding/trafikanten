package no.dkit.android.pendlerkompis.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import no.dkit.android.pendlerkompis.Config;
import no.dkit.android.pendlerkompis.R;

public class StartActivity extends Activity {
    SharedPreferences prefs;
    static String stoppestederString = "";

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.start);
        Button lag = (Button) findViewById(R.id.EndreStopp);
        Button vis = (Button) findViewById(R.id.VisStopp);
        Button hjelp = (Button) findViewById(R.id.VisHjelp);

        lag.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), EndreStoppesteder.class);
                startActivityForResult(myIntent, 0);
            }
        });

        vis.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), VisStoppesteder.class);
                startActivityForResult(myIntent, 0);
            }
        });

        hjelp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), VisHjelp.class);
                startActivityForResult(myIntent, 0);
            }
        });

        oppdaterVisKnapp();
    }

    @Override
    protected void onActivityResult(int i, int i1, Intent intent) {
        super.onActivityResult(i, i1, intent);
        oppdaterVisKnapp();
    }

    private void oppdaterVisKnapp() {
        Button vis = (Button) findViewById(R.id.VisStopp);

        prefs = getSharedPreferences(Config.PREFS_NAME, 0);
        stoppestederString = prefs.getString(Config.PREFS_STOPPESTEDER, "");

        if (stoppestederString.length() == 0) {
            vis.setEnabled(false);
        } else {
            vis.setEnabled(true);
        }
    }
}
