package no.dkit.android.pendlerkompis.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import no.dkit.android.pendlerkompis.Config;
import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.gui.EndreStoppestederAdapter;
import no.dkit.android.pendlerkompis.http.AsyncHttpRequest;
import no.dkit.android.pendlerkompis.json.PlacesParser;
import no.dkit.android.pendlerkompis.json.dto.Places;
import no.dkit.android.pendlerkompis.util.TextHelper;

public class EndreStoppesteder extends ListActivity {
    ArrayList<Places> stoppestedListe = new ArrayList<Places>();
    ArrayList<Places> lagretStoppestedListe = new ArrayList<Places>();
    EditText sokView;
    Button lagre;
    Button sok;
    static String sokeStreng = "";

    EndreStoppesteder myContext;

    static SharedPreferences prefs;

    EndreStoppestederAdapter<Places> stoppestedAdapter;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        myContext = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.endrestoppesteder);

        stoppestedAdapter = new EndreStoppestederAdapter<Places>(myContext, 0);

        sokView = (EditText) findViewById(R.id.SokStreng);
        sok = (Button) findViewById(R.id.Sok);
        lagre = (Button) findViewById(R.id.Lagre);

        sok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    sokeStreng = URLEncoder.encode(sokView.getText().toString(), AsyncHttpRequest.ENCODING).replaceAll("\\+", "%20");
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException("Klarte ikke tolke søk");
                }
                sokStoppesteder();
                //stoppestedFilter.setEnabled(true);
            }
        });

        lagre.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                lagreStoppesteder();
                hentLagredeStoppesteder();
            }
        });

        hentLagredeStoppesteder();
    }

    private void lagreStoppesteder() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Config.PREFS_STOPPESTEDER, TextHelper.stoppestederTilString(stoppestedAdapter.getLagredeStoppesteder()));
        editor.commit();

        Toast.makeText(this, "Stoppestedet er lagret! Du kan nå trykke på tilbakeknappen og velge dette stoppestedet under 'Vis Stoppesteder' - eller lage en Widget på skrivebordet for dette stoppestedet.", Toast.LENGTH_LONG).show();
    }

    private void sokStoppesteder() {
        final ProgressDialog dialog = ProgressDialog.show(EndreStoppesteder.this, getString(R.string.wait_soker_stoppesteder),
                getString(R.string.wait_soker_stoppesteder), true, true);

        new AsyncHttpRequest(Config.STOPPESTED_URL + sokeStreng, new Handler() {
            public void handleMessage(Message msg) {
                String stoppestedJson = msg.getData().getString(AsyncHttpRequest.RESPONSE);
                if (stoppestedJson.equals("")) {
                    dialog.dismiss();
                    new AlertDialog.Builder(myContext)
                            .setMessage(getResources().getText(R.string.network_error))
                            .setNeutralButton(getResources().getText(R.string.ok), null)
                            .show();
                } else {
                    setupStoppestedListe(dialog, stoppestedJson);
                }
            }
        });
    }

    private void setupStoppestedListe(final ProgressDialog dialog, final String stoppestedJson) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                stoppestedListe = new PlacesParser().parseData(stoppestedJson);

                if (stoppestedListe.size() == 0) {
                    dialog.dismiss();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(myContext)
                                    .setMessage(getResources().getText(R.string.ingen_stoppesteder))
                                    .setNeutralButton(getResources().getText(R.string.ok), null)
                                    .show();
                        }
                    });
                } else {
                    stoppestedAdapter.setStoppesteder(stoppestedListe);
                    stoppestedAdapter.setLagredeStoppesteder(lagretStoppestedListe);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            stoppestedAdapter.notifyDataSetChanged();
                            Toast.makeText(EndreStoppesteder.this, stoppestedListe.size() +
                                    " stoppesteder funnet! Stjernemerk de du vil bruke og trykk 'Lagre' for å lagre stoppesteder.", Toast.LENGTH_LONG).show();
                        }
                    });
                    dialog.dismiss();
                }

                return null;
            }
        }.execute();
    }

    private void hentLagredeStoppesteder() {
        prefs = getSharedPreferences(Config.PREFS_NAME, 0);
        String stoppestedString = prefs.getString(Config.PREFS_STOPPESTEDER, "");

        lagretStoppestedListe = TextHelper.stringTilStoppesteder(stoppestedString);
        stoppestedListe.clear();
        stoppestedListe.addAll(lagretStoppestedListe);

        stoppestedAdapter.setStoppesteder(stoppestedListe);
        stoppestedAdapter.setLagredeStoppesteder(lagretStoppestedListe);

        setListAdapter(stoppestedAdapter);
    }
}
