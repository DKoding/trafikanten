
package no.dkit.android.pendlerkompis.json.dto;

import java.util.List;

public class Extensions{
   	private List<Deviations> deviations;
   	private boolean isHub;
   	private String lineColour;
   	private OccupancyData occupancyData;

 	public List<Deviations> getDeviations(){
		return this.deviations;
	}
	public void setDeviations(List<Deviations> deviations){
		this.deviations = deviations;
	}
 	public boolean getIsHub(){
		return this.isHub;
	}
	public void setIsHub(boolean isHub){
		this.isHub = isHub;
	}
 	public String getLineColour(){
		return this.lineColour;
	}
	public void setLineColour(String lineColour){
		this.lineColour = lineColour;
	}
 	public OccupancyData getOccupancyData(){
		return this.occupancyData;
	}
	public void setOccupancyData(OccupancyData occupancyData){
		this.occupancyData = occupancyData;
	}
}
