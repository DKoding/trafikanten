package no.dkit.android.pendlerkompis;

public class Config {
    public static final String ID_KEY = "fromID";
    public static final String NAME_KEY = "fromNavn";
    public static final String RESPONSE_KEY = "response";

    public static final String SIRI_HOST_NAME = "http://reisapi.ruter.no/";
    public static final String AVGANGER_URL = SIRI_HOST_NAME + "StopVisit/GetDepartures/";
    public static final String STOPPESTED_URL = SIRI_HOST_NAME + "Place/GetPlaces/";

    public static final String PREFS_NAME = "Trafikanten-Prefs";
    public static final String PREFS_STOPPESTEDER = "stoppesteder";
    public static final String WIDGET_STOP_ID = "widgetStopId";
    public static final String WIDGET_STOP_NAME = "widgetStopName";
    public static final String WIDGET_CONFIG_PREFIX = "widgetConfig";
}
