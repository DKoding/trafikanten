
package no.dkit.android.pendlerkompis.json.dto;

import java.util.Date;
import java.util.List;

public class MonitoredCall{
   	private String aimedArrivalTime;
   	private String aimedDepartureTime;
   	private String departurePlatformName;
   	private String destinationDisplay;
   	private String expectedArrivalTime;
   	private String expectedDepartureTime;
   	private boolean vehicleAtStop;
   	private Number visitNumber;

 	public String getAimedArrivalTime(){
		return this.aimedArrivalTime;
	}
	public void setAimedArrivalTime(String aimedArrivalTime){
		this.aimedArrivalTime = aimedArrivalTime;
	}
 	public String getAimedDepartureTime(){
		return this.aimedDepartureTime;
	}
	public void setAimedDepartureTime(String aimedDepartureTime){
		this.aimedDepartureTime = aimedDepartureTime;
	}
 	public String getDeparturePlatformName(){
		return this.departurePlatformName;
	}
	public void setDeparturePlatformName(String departurePlatformName){
		this.departurePlatformName = departurePlatformName;
	}
 	public String getDestinationDisplay(){
		return this.destinationDisplay;
	}
	public void setDestinationDisplay(String destinationDisplay){
		this.destinationDisplay = destinationDisplay;
	}
 	public String getExpectedArrivalTime(){
		return this.expectedArrivalTime;
	}
	public void setExpectedArrivalTime(String expectedArrivalTime){
		this.expectedArrivalTime = expectedArrivalTime;
	}
 	public boolean getVehicleAtStop(){
		return this.vehicleAtStop;
	}
	public void setVehicleAtStop(boolean vehicleAtStop){
		this.vehicleAtStop = vehicleAtStop;
	}
 	public Number getVisitNumber(){
		return this.visitNumber;
	}
	public void setVisitNumber(Number visitNumber){
		this.visitNumber = visitNumber;
	}

    public String getExpectedDepartureTime() {
        return expectedDepartureTime;
    }

    public void setExpectedDepartureTime(String expectedDepartureTime) {
        this.expectedDepartureTime = expectedDepartureTime;
    }
}
