package no.dkit.android.pendlerkompis.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;
import no.dkit.android.pendlerkompis.Config;
import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.activity.VisAvganger;

public class TrafikantenWidget extends AppWidgetProvider {
    private static final String AVGANGER_ACTION = "no.dkit.android.pendlerkompis.widget.TrafikantenWidget.VisAvganger";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget);

        for (int appWidgetId : appWidgetIds) {
            if (appWidgetManager.getAppWidgetInfo(appWidgetId) != null) {
                updateWidget(context, remoteViews, appWidgetId, appWidgetManager);
            }
        }
    }

    public static void updateWidget(Context context, RemoteViews remoteViews, int appWidgetId, AppWidgetManager appWidgetManager) {
        SharedPreferences prefs = context.getSharedPreferences(Config.WIDGET_CONFIG_PREFIX + appWidgetId, Context.MODE_PRIVATE);
        Integer stopId = prefs.getInt(Config.WIDGET_STOP_ID, -1);
        String stopName = prefs.getString(Config.WIDGET_STOP_NAME, "");

        remoteViews.setTextViewText(R.id.StoppeSted, stopName);

        Intent intent = new Intent(context, VisAvganger.class);
        intent.setAction(AVGANGER_ACTION+appWidgetId);
        intent.putExtra(Config.ID_KEY, stopId);
        intent.putExtra(Config.NAME_KEY, stopName);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.WidgetView, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }
}
