
package no.dkit.android.pendlerkompis.json.dto;

public class Avgang{
   	private Extensions extensions;
   	private MonitoredVehicleJourney monitoredVehicleJourney;
   	private String monitoringRef;
   	private String recordedAtTime;

 	public Extensions getExtensions(){
		return this.extensions;
	}
	public void setExtensions(Extensions extensions){
		this.extensions = extensions;
	}
 	public MonitoredVehicleJourney getMonitoredVehicleJourney(){
		return this.monitoredVehicleJourney;
	}
	public void setMonitoredVehicleJourney(MonitoredVehicleJourney monitoredVehicleJourney){
		this.monitoredVehicleJourney = monitoredVehicleJourney;
	}
 	public String getMonitoringRef(){
		return this.monitoringRef;
	}
	public void setMonitoringRef(String monitoringRef){
		this.monitoringRef = monitoringRef;
	}
 	public String getRecordedAtTime(){
		return this.recordedAtTime;
	}
	public void setRecordedAtTime(String recordedAtTime){
		this.recordedAtTime = recordedAtTime;
	}
}
