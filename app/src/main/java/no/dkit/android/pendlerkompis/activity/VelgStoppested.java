package no.dkit.android.pendlerkompis.activity;

import android.app.ListActivity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.RemoteViews;

import java.util.ArrayList;

import no.dkit.android.pendlerkompis.Config;
import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.gui.VisStoppestederAdapter;
import no.dkit.android.pendlerkompis.json.dto.Places;
import no.dkit.android.pendlerkompis.util.TextHelper;
import no.dkit.android.pendlerkompis.widget.TrafikantenWidget;

public class VelgStoppested extends ListActivity {
    AppWidgetManager appWidgetManager;
    ArrayList<Places> stoppestedListe = new ArrayList<Places>();

    VelgStoppested myContext;

    SharedPreferences prefs;
    SharedPreferences widgetPrefs;
    int appwidgetId;

    VisStoppestederAdapter<Places> stoppestedAdapter;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        myContext = this;
        appWidgetManager = AppWidgetManager.getInstance(myContext);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.visstoppesteder);
        setResult(RESULT_CANCELED);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            appwidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        if (appwidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        } else {
            stoppestedAdapter = new VisStoppestederAdapter<Places>(myContext, 0);
            widgetPrefs = getSharedPreferences(Config.WIDGET_CONFIG_PREFIX + appwidgetId, Context.MODE_PRIVATE);
            hentLagredePlaceser();
        }
    }

    private void hentLagredePlaceser() {
        prefs = getSharedPreferences(Config.PREFS_NAME, 0);
        String stoppestedString = prefs.getString(Config.PREFS_STOPPESTEDER, "");

        stoppestedListe = TextHelper.stringTilStoppesteder(stoppestedString);
        stoppestedAdapter.setStoppesteder(stoppestedListe);

        setListAdapter(stoppestedAdapter);
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        SharedPreferences.Editor editor = widgetPrefs.edit();
        editor.putInt(Config.WIDGET_STOP_ID, stoppestedAdapter.getItem(position).getiD());
        editor.putString(Config.WIDGET_STOP_NAME, stoppestedAdapter.getItem(position).getName());
        editor.commit();

        RemoteViews views = new RemoteViews(myContext.getPackageName(), R.layout.widget);
        appWidgetManager.updateAppWidget(appwidgetId, views);
        TrafikantenWidget.updateWidget(myContext, views, appwidgetId, appWidgetManager);

        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appwidgetId);
        setResult(RESULT_OK, resultValue);
        finish();
    }
}