
package no.dkit.android.pendlerkompis.json.dto;

import java.util.List;

public class FramedVehicleJourneyRef{
   	private String dataFrameRef;
   	private String datedVehicleJourneyRef;

 	public String getDataFrameRef(){
		return this.dataFrameRef;
	}
	public void setDataFrameRef(String dataFrameRef){
		this.dataFrameRef = dataFrameRef;
	}
 	public String getDatedVehicleJourneyRef(){
		return this.datedVehicleJourneyRef;
	}
	public void setDatedVehicleJourneyRef(String datedVehicleJourneyRef){
		this.datedVehicleJourneyRef = datedVehicleJourneyRef;
	}
}
