package no.dkit.android.pendlerkompis.gui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

import no.dkit.android.pendlerkompis.R;
import no.dkit.android.pendlerkompis.json.dto.Places;

public class EndreStoppestederAdapter<T extends Places> extends StoppestedAdapter<T> {
    public static final int TABLEROW = 0;
    public static final int NAVN = 0;
    public static final int DISTRIKT = 1;
    public static final int BUTTON = 2;

    public EndreStoppestederAdapter(Context context, int i) {
        super(context, i);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TableLayout tableLayout;

        if (convertView == null) {
            tableLayout = (TableLayout) View.inflate(getContext(), R.layout.endrestoppesteder_item, null);
        } else {
            tableLayout = (TableLayout) convertView;
        }

        final TableRow tableRow = (TableRow) tableLayout.getChildAt(TABLEROW);

        final TextView navn = (TextView) tableRow.getChildAt(NAVN);
        final TextView distrikt = (TextView) tableRow.getChildAt(DISTRIKT);
        final Button button = ((Button) tableRow.getChildAt(BUTTON));

        final T stoppested;

        stoppested = stoppestedListe.get(position);

        navn.setText(stoppested.getName());
        distrikt.setText(stoppested.getDistrict());

        tableLayout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (lagredeStoppesteder.contains(stoppested)) {
                    lagredeStoppesteder.remove(stoppested);
                } else {
                    lagredeStoppesteder.add(stoppested);
                }

                notifyDataSetChanged();
            }
        });
        
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (lagredeStoppesteder.contains(stoppested)) {
                    lagredeStoppesteder.remove(stoppested);
                } else {
                    lagredeStoppesteder.add(stoppested);
                }

                notifyDataSetChanged();
            }
        });

        if (lagredeStoppesteder.contains(stoppested)) {
            button.setBackgroundResource(android.R.drawable.btn_star_big_on);
        } else {
            button.setBackgroundResource(android.R.drawable.btn_star_big_off);
        }

        if (position % 2 == 0)
            tableLayout.setBackgroundColor(getContext().getResources().getColor(R.color.linecolor));
        if (position % 2 == 1)
            tableLayout.setBackgroundColor(getContext().getResources().getColor(R.color.altlinecolor));

        return tableLayout;
    }
}