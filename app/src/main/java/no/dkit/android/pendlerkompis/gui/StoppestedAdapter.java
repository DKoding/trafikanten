package no.dkit.android.pendlerkompis.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import no.dkit.android.pendlerkompis.json.dto.Places;

public abstract class StoppestedAdapter<T extends Places> extends ArrayAdapter<T> {
    protected ArrayList<T> stoppestedListe;
    protected ArrayList<T> lagredeStoppesteder;
    LayoutInflater inflater;

    public StoppestedAdapter(Context context, int i) {
        super(context, i);
        inflater = LayoutInflater.from(context);
        stoppestedListe = new ArrayList<T>();
        lagredeStoppesteder = new ArrayList<T>();
    }

    public StoppestedAdapter(Context context, int i, int i1) {
        super(context, i, i1);
        inflater = LayoutInflater.from(context);
        stoppestedListe = new ArrayList<T>();
        lagredeStoppesteder = new ArrayList<T>();
    }

    public StoppestedAdapter(Context context, int i, T[] objects) {
        super(context, i, objects);
        inflater = LayoutInflater.from(context);
        stoppestedListe = new ArrayList<T>();
        lagredeStoppesteder = new ArrayList<T>();
    }

    public StoppestedAdapter(Context context, int i, int i1, T[] objects) {
        super(context, i, i1, objects);
        inflater = LayoutInflater.from(context);
        stoppestedListe = new ArrayList<T>();
        lagredeStoppesteder = new ArrayList<T>();
    }

    public StoppestedAdapter(Context context, int i, ArrayList<T> list) {
        super(context, i, list);
        inflater = LayoutInflater.from(context);
        stoppestedListe = new ArrayList<T>();
        lagredeStoppesteder = new ArrayList<T>();
    }

    public StoppestedAdapter(Context context, int i, int i1, ArrayList<T> list) {
        super(context, i, i1, list);
        inflater = LayoutInflater.from(context);
        stoppestedListe = new ArrayList<T>();
        lagredeStoppesteder = new ArrayList<T>();
    }

    public void setStoppesteder(ArrayList<T> list) {
        stoppestedListe.clear();
        stoppestedListe.addAll(list);
    }

    public void setLagredeStoppesteder(ArrayList<T> list) {
        lagredeStoppesteder.clear();
        lagredeStoppesteder.addAll(list);
    }

    public int getCount() {
        return stoppestedListe.size();
    }

    public T getItem(int position) {
        return stoppestedListe.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public ArrayList<T> getLagredeStoppesteder() {
        return lagredeStoppesteder;
    }

    public abstract View getView(int position, View convertView, ViewGroup parent);
}