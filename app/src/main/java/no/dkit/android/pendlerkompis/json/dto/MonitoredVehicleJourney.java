
package no.dkit.android.pendlerkompis.json.dto;

public class MonitoredVehicleJourney{
   	private String blockRef;
   	private String delay;
   	private String destinationAimedArrivalTime;
   	private String destinationName;
   	private Number destinationRef;
   	private String directionName;
   	private String directionRef;
   	private FramedVehicleJourneyRef framedVehicleJourneyRef;
   	private boolean inCongestion;
   	private String lineRef;
   	private boolean monitored;
   	private MonitoredCall monitoredCall;
   	private String operatorRef;
   	private String originAimedDepartureTime;
   	private String originName;
   	private String originRef;
   	private String publishedLineName;
   	private String trainBlockPart;
   	private String vehicleFeatureRef;
   	private String vehicleJourneyName;
   	private Number vehicleMode;
   	private String vehicleRef;

 	public String getBlockRef(){
		return this.blockRef;
	}
	public void setBlockRef(String blockRef){
		this.blockRef = blockRef;
	}
 	public String getDelay(){
		return this.delay;
	}
	public void setDelay(String delay){
		this.delay = delay;
	}
 	public String getDestinationAimedArrivalTime(){
		return this.destinationAimedArrivalTime;
	}
	public void setDestinationAimedArrivalTime(String destinationAimedArrivalTime){
		this.destinationAimedArrivalTime = destinationAimedArrivalTime;
	}
 	public String getDestinationName(){
		return this.destinationName;
	}
	public void setDestinationName(String destinationName){
		this.destinationName = destinationName;
	}
 	public Number getDestinationRef(){
		return this.destinationRef;
	}
	public void setDestinationRef(Number destinationRef){
		this.destinationRef = destinationRef;
	}
 	public String getDirectionName(){
		return this.directionName;
	}
	public void setDirectionName(String directionName){
		this.directionName = directionName;
	}
 	public String getDirectionRef(){
		return this.directionRef;
	}
	public void setDirectionRef(String directionRef){
		this.directionRef = directionRef;
	}
 	public FramedVehicleJourneyRef getFramedVehicleJourneyRef(){
		return this.framedVehicleJourneyRef;
	}
	public void setFramedVehicleJourneyRef(FramedVehicleJourneyRef framedVehicleJourneyRef){
		this.framedVehicleJourneyRef = framedVehicleJourneyRef;
	}
 	public boolean getInCongestion(){
		return this.inCongestion;
	}
	public void setInCongestion(boolean inCongestion){
		this.inCongestion = inCongestion;
	}
 	public String getLineRef(){
		return this.lineRef;
	}
	public void setLineRef(String lineRef){
		this.lineRef = lineRef;
	}
 	public boolean getMonitored(){
		return this.monitored;
	}
	public void setMonitored(boolean monitored){
		this.monitored = monitored;
	}
 	public MonitoredCall getMonitoredCall(){
		return this.monitoredCall;
	}
	public void setMonitoredCall(MonitoredCall monitoredCall){
		this.monitoredCall = monitoredCall;
	}
 	public String getOperatorRef(){
		return this.operatorRef;
	}
	public void setOperatorRef(String operatorRef){
		this.operatorRef = operatorRef;
	}
 	public String getOriginAimedDepartureTime(){
		return this.originAimedDepartureTime;
	}
	public void setOriginAimedDepartureTime(String originAimedDepartureTime){
		this.originAimedDepartureTime = originAimedDepartureTime;
	}
 	public String getOriginName(){
		return this.originName;
	}
	public void setOriginName(String originName){
		this.originName = originName;
	}
 	public String getOriginRef(){
		return this.originRef;
	}
	public void setOriginRef(String originRef){
		this.originRef = originRef;
	}
 	public String getPublishedLineName(){
		return this.publishedLineName;
	}
	public void setPublishedLineName(String publishedLineName){
		this.publishedLineName = publishedLineName;
	}
 	public String getTrainBlockPart(){
		return this.trainBlockPart;
	}
	public void setTrainBlockPart(String trainBlockPart){
		this.trainBlockPart = trainBlockPart;
	}
 	public String getVehicleFeatureRef(){
		return this.vehicleFeatureRef;
	}
	public void setVehicleFeatureRef(String vehicleFeatureRef){
		this.vehicleFeatureRef = vehicleFeatureRef;
	}
 	public String getVehicleJourneyName(){
		return this.vehicleJourneyName;
	}
	public void setVehicleJourneyName(String vehicleJourneyName){
		this.vehicleJourneyName = vehicleJourneyName;
	}
 	public Number getVehicleMode(){
		return this.vehicleMode;
	}
	public void setVehicleMode(Number vehicleMode){
		this.vehicleMode = vehicleMode;
	}
 	public String getVehicleRef(){
		return this.vehicleRef;
	}
	public void setVehicleRef(String vehicleRef){
		this.vehicleRef = vehicleRef;
	}
}
