package no.dkit.android.pendlerkompis.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import no.dkit.android.pendlerkompis.json.dto.Avgang;

public abstract class AvgangAdapter<T extends Avgang> extends ArrayAdapter<T> {
    protected ArrayList<T> avgangListe;
    protected ArrayList<T> filtrerteAvganger;
    LayoutInflater inflater;

    String filter = NO_FILTER;
    protected static final String NO_FILTER = "";

    public AvgangAdapter(Context context, int i) {
        super(context, i);
        inflater = LayoutInflater.from(context);
        avgangListe = new ArrayList<T>();
        filtrerteAvganger = new ArrayList<T>();
    }

    public void setAvganger(ArrayList<T> list) {
        avgangListe.clear();
        avgangListe.addAll(list);
        filtrerteAvganger.clear();
        filter = NO_FILTER;
    }

    public void updateFilteredItems() {
        if (!filter.equals(NO_FILTER)) {
            filtrerteAvganger.clear();
            clear();

            for (T transport : avgangListe) {
                if (transport.getMonitoredVehicleJourney().getDestinationName().toUpperCase().contains(filter) || transport.getMonitoredVehicleJourney().getPublishedLineName().toUpperCase().contains(filter)) {
                    filtrerteAvganger.add(transport);
                    add(transport);
                }
            }
        }
        notifyDataSetChanged();
    }

    public int getCount() {
        return filter.equals(NO_FILTER) ? avgangListe.size() : filtrerteAvganger.size();
    }

    public T getItem(int position) {
        return filter.equals(NO_FILTER) ? avgangListe.get(position) : filtrerteAvganger.get(position);
    }

    public boolean areAllItemsSelectable() {
        return true;
    }

    public long getItemId(int position) {
        return position;
    }

    public void setTextFilter(String filter) {
        this.filter = filter;
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public abstract View getView(int position, View convertView, ViewGroup parent);
}