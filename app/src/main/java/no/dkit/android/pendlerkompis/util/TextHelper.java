package no.dkit.android.pendlerkompis.util;

import no.dkit.android.pendlerkompis.json.dto.Places;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class TextHelper {
    static Date date;
    static final long minute = 60 * 1000;

    static SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    static SimpleDateFormat toFormat = new SimpleDateFormat("HH:mm");

    public static String formatTime(String dateString) {
        try {
            date = fromFormat.parse(dateString.substring(0, 21));
        } catch (ParseException e) {
            return "N/A";
        }

        long minutes = (date.getTime() - System.currentTimeMillis()) / minute;
        return toFormat.format(date) + " (" + minutes + "m)";
    }

    public static ArrayList<Places> stringTilStoppesteder(String stoppestedString) {
        ArrayList<Places> stoppestedListe = new ArrayList<Places>();

        if (stoppestedString.equals("")) return stoppestedListe;

        String[] items = stoppestedString.split(",");
        for (int i = 0; i < items.length; i += 5) {
            stoppestedListe.add(new Places(
                    Integer.parseInt(items[i]),
                    items[i + 1], items[i + 2],
                    0,
                    0
            ));
        }

        return stoppestedListe;
    }

    public static String stoppestederTilString(ArrayList<Places> list) {
        StringBuffer buffer = new StringBuffer(8192);

        if (list != null) {
            final int size = list.size();

            for (int i = 0; i < size; i++) {
                Places stoppested = list.get(i);
                buffer.append(stoppested.getiD()).append(",").
                        append(stoppested.getName()).append(",").
                        append(stoppested.getDistrict()).append(",").
                        append(0).append(",").
                        append(0);
                if (i < size - 1) buffer.append(",");
            }
        }

        return buffer.toString();
    }
}
