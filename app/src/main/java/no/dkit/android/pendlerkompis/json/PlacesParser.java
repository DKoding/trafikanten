package no.dkit.android.pendlerkompis.json;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collection;

import no.dkit.android.pendlerkompis.json.dto.Places;

public class PlacesParser {

    public ArrayList<Places> parseData(String json) {
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        return gson.fromJson(json, new TypeToken<Collection<Places>>() {
        }.getType());
    }
}