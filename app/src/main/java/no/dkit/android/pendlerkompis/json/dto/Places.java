
package no.dkit.android.pendlerkompis.json.dto;

import java.util.List;

public class Places {
    private boolean alightingAllowed;
    private boolean boardingAllowed;
    private List<Deviations> deviations;
    private String district;
    private Integer iD;
    private boolean isHub;
    private List<Lines> lines;
    private String name;
    private String placeType;
    private boolean realTimeStop;
    private String shortName;
    private List<StopPoints> stopPoints;
    private Integer x;
    private Integer y;
    private String zone;

    public Places(Integer iD, String name, String district, Integer x, Integer y) {
        this.iD = iD;
        this.name = name;
        this.district = district;
        this.x = x;
        this.y = y;
    }

    public boolean getAlightingAllowed() {
        return this.alightingAllowed;
    }

    public void setAlightingAllowed(boolean alightingAllowed) {
        this.alightingAllowed = alightingAllowed;
    }

    public boolean getBoardingAllowed() {
        return this.boardingAllowed;
    }

    public void setBoardingAllowed(boolean boardingAllowed) {
        this.boardingAllowed = boardingAllowed;
    }

    public List<Deviations> getDeviations() {
        return this.deviations;
    }

    public void setDeviations(List<Deviations> deviations) {
        this.deviations = deviations;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public boolean getIsHub() {
        return this.isHub;
    }

    public void setIsHub(boolean isHub) {
        this.isHub = isHub;
    }

    public List<Lines> getLines() {
        return this.lines;
    }

    public void setLines(List<Lines> lines) {
        this.lines = lines;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceType() {
        return this.placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public boolean getRealTimeStop() {
        return this.realTimeStop;
    }

    public void setRealTimeStop(boolean realTimeStop) {
        this.realTimeStop = realTimeStop;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public List<StopPoints> getStopPoints() {
        return this.stopPoints;
    }

    public void setStopPoints(List<StopPoints> stopPoints) {
        this.stopPoints = stopPoints;
    }

    public String getZone() {
        return this.zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Integer getiD() {
        return iD;
    }

    public void setiD(Integer iD) {
        this.iD = iD;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
